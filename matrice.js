const fichier_matrice = "matrice_adjacence";

class MatriceAdjacence{
    
    constructor(){
        this.matrice = new Array();;
        this.sommet = new Array();//modif
    }

    affichage(){
        process.stdout.write("\nReprésentation de la matrice d'adjacence :\n");
        process.stdout.write("\t");
        for (let i = 0; i < this.matrice.length; i++){
            process.stdout.write((i+1)+" ");
        }
        process.stdout.write("\n");
        for (let i = 0; i < this.matrice.length; i++){
            process.stdout.write((i+1) +" ( ");
            for (let j = 0; j < this.matrice.length; j++){
                process.stdout.write(this.matrice[i][j] + " ");
            }
            process.stdout.write(")\n");
        }
        process.stdout.write("S (");
        for (let k = 0; k< this.sommet.length; k++){ //modif
            process.stdout.write(" ( Indice : "+ (k+1)+", Nom : " + this.sommet[k]+" )");
            if(k != this.sommet.length-1) process.stdout.write(", \n");
        }
        process.stdout.write(" )\n");
    }

    //Question 2
    add_sommet(nom_de_sommet_qui_sert_a_rien){
        this.sommet.push(nom_de_sommet_qui_sert_a_rien); //modif
        let new_line = Array(this.matrice.length+1);
        for (let index = 0; index < new_line.length; index++) {
            new_line[index] = 0;
        }
        for (let index = 0; index < this.matrice.length; index++) {
            this.matrice[index].push(0);
        }
        this.matrice.push(new_line);
    }

    //Question 3
    add(i,j){
        if (i > this.matrice.length && j > this.matrice.length)
            console.error("Index i or j out of matrix's range");
        if (i == j)
            console.error("Cannot create an edge between a single vertex");
        else{
            this.matrice[i-1][j-1] = 1;
            this.matrice[j-1][i-1] = 1;
        }
    }

    //Question 4
    supp(i,j){
        if (i > this.matrice.length && j > this.matrice.length)
            console.error("Index i or j out of matrix's range");
        if (i == j)
            console.error("Cannot delete an edge between a single vertex");
        if (this.matrice[j-1][i-1] == 0 && this.matrice[i-1][j-1] == 0)
            console.error("Cannot delete an edge which doesn't exist");
        else{
            this.matrice[i-1][j-1] = 0;
            this.matrice[j-1][i-1] = 0;
        }
    }

    //Question 5
    est_voisin(i,j){
        if (i > this.matrice.length && j > this.matrice.length)
            console.error("Index i or j out of matrix's range");
        else{
            if(this.matrice[i-1][j-1] == 1) return true
            else return false
        }
        
    }

    //Question 6
    load(fichier_matrice){
        const fs = require('fs')
        const data = JSON.parse(fs.readFileSync(fichier_matrice, 'utf-8'))
        this.matrice = new Array();
        for(let i = 0; i < data.length; i++){
            this.matrice.push(data[i]);
        }
    }

    //Question 7
    save(fichier_matrice){
        const jsonData = JSON.stringify(this.matrice);
        var fs = require('fs');
        fs.writeFileSync(fichier_matrice, jsonData);
    }
}

module.exports = {
    MatriceAdjacence   
}