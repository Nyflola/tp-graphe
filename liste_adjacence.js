
class List_node{
    constructor(nom) {
        this.nom = nom;
        this.suivant = null;                
    }
}

class Linked_list{
    constructor(head = null){
        this.head = head;
    }
    append(value, nom = null){
        if(this.head == null){
            this.head = new List_node(value);
        }
        else{
            let ptr = this.head;
            while(ptr.nom != value && ptr.suivant != null){
                ptr = ptr.suivant;
            }
            if(ptr.nom != value){
                ptr.suivant = new List_node(value);
            } 
            else{
                //if(nom) console.error("Le sommet "+ nom +" est déjà un voisin du sommet " + value);
            } 
        }
    }
    delete(edge){
        if(this.head){
            let prev = this.head;
            let ptr = this.head.suivant;
            if(this.head.nom == edge){
                this.head = this.head.suivant;
            }
            else{
                while(ptr){
                    if(ptr.nom == edge){
                        prev.suivant = ptr.suivant;
                    }
                }
                prev = ptr;
                ptr = ptr.suivant;
            }
        }
    }
    find(edge){
        if(this.head){
            let prev = this.head;
            let ptr = this.head.suivant;
            if(this.head.nom == edge){
                return 1;
            }
            else{
                while(ptr){
                    if(ptr.nom == edge){
                        return 1;
                    }
                }
                prev = ptr;
                ptr = ptr.suivant;
            }
        }
        return 0;
    }
    est_present(edge){
        if(this.head){
            let ptr = this.head;
                while(ptr){
                    if(ptr.nom == edge){
                        return 1;
                    } else {
                        ptr = ptr.suivant;
                    }
                }
            return 0;
        }
    }   
    print(){
        console.log(this.head)
    }
}

class Maillon_liste_adjacence{
    constructor(data){
        this.nom = data;
        this.voisin = new Linked_list();
        this.suivant = null
    }
}

class Liste_Adjacence{
    constructor(){
        this.head = null;
    }

    add_vertex(vertex){
        if(this.head == null){
            this.head = new Maillon_liste_adjacence(vertex);
        } 
        else{
            let ptr = this.head;
            while(ptr.nom != vertex && ptr.suivant != null) ptr = ptr.suivant;
            if(ptr.nom != vertex) ptr.suivant = new Maillon_liste_adjacence(vertex);
            else console.error("Le sommet "+ vertex +" existe déjà");
        }
    }

    add_edge(from,to){
        let ptrFrom = null;
        let ptrTo = null;
        let ptr = this.head;
        while(ptr && (!ptrTo || !ptrFrom)){
            if(ptr.nom == from) ptrFrom = ptr;
            if(ptr.nom == to) ptrTo = ptr;
            ptr = ptr.suivant;
        }
        if(ptrTo && ptrFrom){
            ptrFrom.voisin.append(to,ptrFrom.nom);
            ptrTo.voisin.append(from,ptrTo.nom);
        }
        if(!ptrFrom){
            console.error("Le sommet " + from + " n'est pas dans le graphe.");
        }
        if(!ptrTo){
            console.error("Le sommet " + to + " n'est pas dans le graphe.");
        }
    }

    nb_sommets(){
        let ptr = this.head;
        let nombre = 0;
        while(ptr){
            nombre++;
            ptr = ptr.suivant;
        }
        return nombre;
    }

    supp_edge(from,to){
        let ptr = this.head;
        while(ptr){
            if(ptr.nom == from){
                ptr.voisin.delete(to);     
            }
            if(ptr.nom == to){
                ptr.voisin.delete(from);
            }
            ptr = ptr.suivant;
        }
    }

    est_voisin(from, to){
        let ptr = this.head;
        while(ptr){
            if(ptr.nom == from){
                return console.log(ptr.voisin.find(to));     
            }
            if(ptr.nom == to){
                return console.log(ptr.voisin.find(from));
            }
            ptr = ptr.suivant;
        }
    }

    load(fichier_liste){
        const fs = require('fs');
        const data = fs.readFileSync(fichier_liste, 'utf-8');
        const n = data[0];
        this.head = null;
        let j = 4;
        for(let i = 0; i < n; i++){
            this.add_vertex(data[j]);
            j = j + 4;
        }
        while(j < data.length){
            this.add_edge(data[j-2], data[j]);
            j = j + 4;
        }
    }

    save(fichier_liste){
        var fs = require('fs');
        var Data = (this.nb_sommets()).toString().concat("\n");
        let tab = new Array();
        let ptr = this.head;
        while(ptr){
            tab.push(ptr.nom);
            ptr = ptr.suivant;
        }
        for(let i = 1; i <= tab.length; i++){
            Data = Data.concat(i.toString() + " "+ tab[i-1] + "\n");
        }
        ptr = this.head;
        while(ptr){
            let ptv = ptr.voisin.head;
            while(ptv){
                Data = Data.concat(ptr.nom + " " + ptv.nom + "\n");
                ptv = ptv.suivant;
            }
            ptr = ptr.suivant;
        }
        fs.writeFileSync(fichier_liste, Data);
    }

    est_complet(){
        let ptr = this.head;
        let tab = new Array();
        while(ptr){
            tab.push(ptr.nom);
            ptr = ptr.suivant;
        }
        ptr = this.head;
        while(ptr){
            let ptv = ptr.voisin;
            for(let i = 0; i < tab.length; i++){
                if(ptr.nom == tab[i]){
                    continue;
                } else {                        
                    if(ptv.est_present(tab[i]) == 1){
                        continue;
                    } else {
                        return 0;
                    }
                }
            }
            ptr = ptr.suivant;
        }
        return 1;
    }

    print(){
        process.stdout.write("\nReprésentation de la liste d'adjacence :\n");
        process.stdout.write("{\n");
        let ptr = this.head;
        while(ptr){
            process.stdout.write("( " + ptr.nom + " : ");
            let ptr2 = ptr.voisin.head;
            if(ptr2){
                while(ptr2){
                    process.stdout.write(ptr2.nom);
                    if(ptr2.suivant) process.stdout.write(", ");
                    else process.stdout.write(" )\n");
                    ptr2 = ptr2.suivant;
                }
            }
            else process.stdout.write("pas de voisins )\n");
            ptr = ptr.suivant;
        }
        process.stdout.write("}\n");
        //console.log(this.head);
    }
}

module.exports = {
    Liste_Adjacence,
}