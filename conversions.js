const matrix = require('./matrice')
const list = require('./liste_adjacence')

function liste_to_matrice(liste){
    const matrice = new matrix.MatriceAdjacence();
    let ptr = liste.head;
    while(ptr){
        matrice.add_sommet(ptr.nom);
        ptr = ptr.suivant;   
    } 
    ptr = liste.head;
    while(ptr){
        if(ptr.voisin != null){ 
            let ptr2 = ptr.voisin.head;
            while(ptr2){
                matrice.add(matrice.sommet.indexOf(ptr2.nom)+1,matrice.sommet.indexOf(ptr.nom)+1);
                ptr2 = ptr2.suivant;
            }
        }
        ptr = ptr.suivant;
    }
    return matrice;
}
function matrice_to_liste(matrice){
    const liste = new list.Liste_Adjacence();
    for (let i = 0; i < matrice.sommet.length; i++){
        liste.add_vertex(matrice.sommet[i]);
    }
    for (let i = 0; i < matrice.matrice.length; i++){          
        for (let j = 0; j < matrice.matrice.length; j++){
            if(matrice.matrice[i][j] == 1){
                liste.add_edge(matrice.sommet[i],matrice.sommet[j]);
            }
        }
    }
    return liste;
}

module.exports = {
    matrice_to_liste,
    liste_to_matrice
}