const matrix = require('./matrice');
const list = require('./liste_adjacence');
const ext = require('./conversions.js'); 

/*
********************************************
*       Main pour matrice d'adjacence      *
********************************************
*/

console.log("********************************************");
console.log("*       Exemple matrice d'adjacence        *");
console.log("********************************************");
//Instanciation d'une nouvelle matrice
const matrice = new matrix.MatriceAdjacence();
//Ajout de quelques sommets
matrice.add_sommet("A");
matrice.add_sommet("B");
matrice.add_sommet("C");
matrice.add_sommet("D");
//Ajout de quelques arrêtes
matrice.add(1,2);
matrice.add(1,3);
matrice.add(1,4);
//Affichage
matrice.affichage();
//conversion
const l = ext.matrice_to_liste(matrice);
l.print();

/*
********************************************
*       Main pour liste d'adjacence      *
********************************************
*/

console.log("\n\n");
console.log("********************************************");
console.log("*         Exemple liste d'adjacence        *");
console.log("********************************************");
//Instanciation d'une liste d'adjacence 
const liste = new list.Liste_Adjacence();
//Ajout de quelques sommets
liste.add_vertex("A");
liste.add_vertex("B");
liste.add_vertex("C");
liste.add_vertex("D");
liste.add_vertex("E");
//Ajout de quelques arrêtes
liste.add_edge("A","C");
liste.add_edge("A","D");
liste.add_edge("B","C");
//Conversion en matrice
//const m = ext.liste_to_matrice(liste);
//liste.print();
//m.affichage();
